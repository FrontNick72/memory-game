// cards array holds all cards
let cards = [];

// deck of all cards in game
const deck = document.querySelector('.deck');

// declaring move variable
let moves = 0;
const counter = document.querySelector('.moves');

// declaring variable of matchedCards
const matchedCard = document.getElementsByClassName('match');

// declare modal
const modalFinish = document.getElementById('popup-win');
const modalStart = document.getElementById('popup-start');

const btnStart = document.querySelector('.btn-start-game');
const btnRestart = document.querySelector('.restart');

// array for opened cards
let openedCards = [];

// @description game timer
let second = 0;
let interval;
const timer = document.querySelector('.timer');

const createCard = (index) => {
  const cardElement = document.createElement('li');
  cardElement.className = 'deck__card card';
  cardElement.innerHTML = index;
  return cardElement;
};

const fillDeck = (countCard) => {
  cards = [];

  for (let index = 1; index <= countCard; index++) {
    for (let i = 0; i < 2; i++) {
      const element = createCard(index);
      cards.push(element);
    }
  }
};

// @description toggles open and show class to display cards
const displayCard = function () {
  this.classList.toggle('open');
  this.classList.toggle('show');
  this.classList.toggle('disabled');
};

// @desciption for user to play Again
const playAgain = () => {
  modalFinish.classList.remove('show');
  modalStart.classList.add('show');
}

// @description shuffles cards
// @param {array}
// @returns shuffledarray
function shuffle(array) {
  let currentIndex = array.length;

  while (currentIndex !== 0) {
    const randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    const temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

// @description function to start a new play
function startGame(countCard) {
  modalStart.classList.remove('show');
  // empty the openCards array
  openedCards = [];

  fillDeck(countCard);

  // shuffle deck
  cards = shuffle(cards);

  // remove all exisiting classes from each card
  for (let i = 0; i < cards.length; i++) {
    deck.innerHTML = '';

    cards.forEach((item) => {
      deck.appendChild(item);
    });

    cards[i].classList.remove('show', 'open', 'match', 'disabled');

    card = cards[i];
    card.addEventListener('click', displayCard);
    card.addEventListener('click', cardOpen);
    card.addEventListener('click', showFinishModal);
  }

  // reset moves
  moves = 0;
  counter.innerHTML = moves + ' moves';

  //reset timer
  second = 0;
  const timer = document.querySelector('.timer');
  timer.innerHTML = '0 secs';
  clearInterval(interval);
}

function handlerStartButton(event) {
  event.preventDefault();

  const inputCount = document.querySelector('.input-count');
  const valueCount = inputCount.value;
  const isValid = valueCount % 2 === 0 && valueCount >= 2 && valueCount <= 10;

  if (!valueCount) {
    inputCount.focus();
    return;
  }

  if (isValid) {
    startGame(valueCount);
  } else {
    startGame(4);
  }
}

// @description add opened cards to OpenedCards list and check if cards are match or not
function cardOpen() {
  openedCards.push(this);
  const lengthCards = openedCards.length;

  if (lengthCards === 2) {
    moveCounter();

    if (Number(openedCards[0].innerHTML) === Number(openedCards[1].innerHTML)) {
      matched();
    } else {
      unmatched();
    }
  }
}

// @description when cards match
function matched() {
  openedCards[0].classList.add('match', 'disabled');
  openedCards[1].classList.add('match', 'disabled');
  openedCards[0].classList.remove('show', 'open', 'no-event');
  openedCards[1].classList.remove('show', 'open', 'no-event');
  openedCards = [];
}

// @description disable cards temporarily
function disable() {
  Array.prototype.filter.call(cards, (card) => {
    card.classList.add('disabled');
  });
}

// description when cards don't match
function unmatched() {
  openedCards[0].classList.add('unmatched');
  openedCards[1].classList.add('unmatched');
  disable();

  setTimeout(() => {
    openedCards[0].classList.remove('show', 'open', 'no-event', 'unmatched');
    openedCards[1].classList.remove('show', 'open', 'no-event', 'unmatched');
    enable();
    openedCards = [];
  }, 1100);
}

// @description enable cards and disable matched cards
function enable() {
  Array.prototype.filter.call(cards, (card) => {
    card.classList.remove('disabled');

    for (let i = 0; i < matchedCard.length; i++) {
      matchedCard[i].classList.add('disabled');
    }
  });
}

// @description count player's moves
function moveCounter() {
  moves++;
  counter.innerHTML = moves + ' moves';
  //start timer on first click
  if (moves === 1) {
    second = 0;
    startTimer();
  }
}

function startTimer() {
  interval = setInterval(() => {
    timer.innerHTML = second + ' secs';
    second++;

    if (second === 60) {
      second = 0;
      playAgain();
    }
  }, 1000);
}

// @description congratulations when all cards match, show modal and moves, time and rating
function showFinishModal() {
  if (matchedCard.length === cards.length) {
    clearInterval(interval);
    const finalTime = timer.innerHTML;

    // show congratulations modal
    modalFinish.classList.add('show');

    //showing move, rating, time on modal
    document.querySelector('.finalMove').innerHTML = moves;
    document.querySelector('.totalTime').innerHTML = finalTime;
  }
}

btnStart.addEventListener('click', handlerStartButton);
btnRestart.addEventListener('click', playAgain);